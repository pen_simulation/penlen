	<?php include 'head.php'; ?>

	<main> 
		<section class="well well2">
	        <div class="container">
	          	<h2 class="text-center">
	            	Product Knowledge
	          	</h2>
	          	<div class="row row_offs1">

		            <ul class="index-list col-4_mod">

			            <li class="col-md-4 col-sm-6 col-xs-6">

			            	<div class="card">
								
								<div class="card-background"
									 style="background-image: url('images/content/content-1.jpg');">

									<span></span>

								</div>

								<div class="card-wrapper">

									<a href="#" class="card-content">
										<div class="card-main">
											<h2>New Product Knowladge 1</h2>

											<p>Lorem ipsum dolor si amet, prostecura ci no falacity</p>
										</div>
									</a>

								</div>

			            	</div>
			                
			            </li>

						<li class="col-md-4 col-sm-6 col-xs-6">

			            	<div class="card">
								
								<div class="card-background"
									 style="background-image: url('images/content/content-2.jpg');">

									<span></span>

								</div>

								<div class="card-wrapper">

									<a href="#" class="card-content">
										<div class="card-main">
											<h2>New Product Knowladge 1</h2>

											<p>Lorem ipsum dolor si amet, prostecura ci no falacity</p>
										</div>
									</a>

								</div>

			            	</div>
			                
			            </li>

			            <li class="col-md-4 col-sm-6 col-xs-6">

			            	<div class="card">
								
								<div class="card-background"
									 style="background-image: url('images/content/content-3.jpg');">

									<span></span>

								</div>

								<div class="card-wrapper">

									<a href="#" class="card-content">
										<div class="card-main">
											<h2>New Product Knowladge 1</h2>

											<p>Lorem ipsum dolor si amet, prostecura ci no falacity</p>
										</div>
									</a>

								</div>

			            	</div>
			                
			            </li>

		            </ul>

	          	</div>
	        </div>        
	    </section>  
	</main>

	<?php include 'footer.php'; ?>