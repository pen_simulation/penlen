  <body>
  <div class="page">
  <!--========================================================
                            HEADER
  =========================================================-->
    <header >  
      <div id="stuck_container" class="stuck_container">
        <nav class="navbar navbar-default navbar-static-top ">
          <div class="container navbar-custom">             
            
            <div class="navbar-header">
              <h1 class="navbar-brand">
                <a  href="index.php">
                  <!-- Add new image logo -->
                  <img src="images/pen-logo.png" alt="Logo Website">
                </a>
              </h1>
            </div>

            <ul class="navbar-nav sf-menu pull-right" data-type="navbar">
              <li class="active">
                <a href="index.php">Home</a>
              </li>
              <li class="dropdown">
                <a class="carret" href="#">Brands</a>
                <ul class="dropdown-menu">
                  <li>
                    <a class="carret" href="#">Catalog</a>
                    <ul class="dropdown-menu">
                      <li>
                        <a class="carret" href="#">Printing</a>
                          <ul class="dropdown-menu">
                            <li>
                              <a href="#">Pen</a>
                            </li>
                            <li>
                              <a href="#">Pen Tail</a>
                            </li>                      
                            <li>
                              <a href="#">Oil Pastel</a>
                            </li>  
                          </ul>
                      </li>
                      <li>
                        <a class="carret" href="#">Retail</a>
                          <ul class="dropdown-menu">
                            <li>
                              <a href="#">Printing</a>
                            </li>
                            <li>
                              <a href="#">Retail</a>
                            </li>                      
                          </ul>
                      </li>                      
                    </ul>
                  </li>
                  <li>
                    <a href="product-knowledge.php">Product Knowledge</a>                      
                  </li>
                  <li>
                    <a href="gallery.php">Gallery</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown">
                <a class="carret" href="#">News &amp; Promotions</a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="news.php">News</a>
                  </li>
                  <li>
                    <a href="product-lunch.php">Product Lunch</a>                      
                  </li>
                  <li>
                    <a href="promotions.php">Promotions</a>
                  </li>
                  <li>
                    <a href="event.php">Event</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="contact-us.php"> Contact Us</a>
              </li>
              <li>
                <a href="cart.php"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></a>
              </li>
              <li>
                <a href="user.php"><i class="fa fa-user" aria-hidden="true"></i></a>
              </li>
              <li>
                <a href="user.php"><i class="fa fa-user" aria-hidden="true"></i></a>
              </li>
            </ul>

          </div>
        </nav>
      </div>  

      <!-- <section class="well well1">
        <div class="container">
          <div class="jumbotron">
            <p>
              Find office space for small businesses,
              <small>
                startups &amp; freelancers - search now!
              </small>
            </p>
          </div>  

          
          <form class="search-form" action="search.php" method="GET" accept-charset="utf-8">
            <label class="search-form_label">
              <input class="search-form_input" type="text" name="s" autocomplete="off" placeholder="Location,  Size,  Cost "/>
              <span class="search-form_liveout"></span>
            </label>
            <button class="search-form_submit btn btn-primary" type="submit">Search</button>
          </form>
          

        </div>
      </section> -->

    </header>