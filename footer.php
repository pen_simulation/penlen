
    <!--========================================================
                            FOOTER
  =========================================================-->
  <footer>

    <section class="well well4">
      <div class="container">
        <div class="row">

          <div class="col-md-4 col-sm-6 col-xs-12">

            <h3 class="white">
              Quick Links
            </h3>  
            <ul class="marked-list marked-list1">
              <li>
                <a href="#"><span>Lorem ipsum dolor sit amet </span></a>
              </li>
              <li>
                <a href="#"><span>Conse ctetur adipisicing </span></a>
              </li>
              <li>
                <a href="#"><span>Elit sed do eiusmod tempor</span></a>
              </li>
              <li>
                <a href="#"><span>Incididunt ut labore</span></a>
              </li>
            </ul>

            <h3 class="white">
             Events
            </h3>  
            <ul class="marked-list marked-list1">
              <li>
                <a href="#"><span>Lorem ipsum dolor sit amet </span></a>
              </li>
              <li>
                <a href="#"><span>Conse ctetur adipisicing </span></a>
              </li>
              <li>
                <a href="#"><span>Elit sed do eiusmod tempor</span></a>
              </li>
              <li>
                <a href="#"><span>Incididunt ut labore</span></a>
              </li>
            </ul>

          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">

            <h3 class="white">
              Services
            </h3>  
            <ul class="marked-list marked-list1">
              <li>
                <a href="#"><span>Lorem ipsum dolor sit amet </span></a>
              </li>
              <li>
                <a href="#"><span>Conse ctetur adipisicing </span></a>
              </li>
              <li>
                <a href="#"><span>Elit sed do eiusmod tempor</span></a>
              </li>
              <li>
                <a href="#"><span>Incididunt ut labore</span></a>
              </li>
            </ul>

            <h3 class="white">
             Events
            </h3>  
            <ul class="marked-list marked-list1">
              <li>
                <a href="#"><span>Lorem ipsum dolor sit amet </span></a>
              </li>
              <li>
                <a href="#"><span>Conse ctetur adipisicing </span></a>
              </li>
              <li>
                <a href="#"><span>Elit sed do eiusmod tempor</span></a>
              </li>
              <li>
                <a href="#"><span>Incididunt ut labore</span></a>
              </li>
            </ul>

          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">

            

            <h3 class="white">
              Follow Us
            </h3>

            <ul>      
              <li class="share">
                <a href="#">
                  <div class="share-inside">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                  </div>
                </a>
              </li>
              <li class="share">
                <a href="#">
                  <div class="share-inside">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                  </div>
                </a>
              </li>              
              <li class="share">
                <a href="#">
                  <div class="share-inside">
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                  </div>
                </a>
              </li>           
            </ul>
            
            <h3 class="white">
              Find A Store
            </h3>
            
            <form>
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                </span>
              </div><!-- /input-group -->
            </form>

            <h3 class="white">
              Newsletter
            </h3>

            <form>
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Enter your E-mail address">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                </span>
              </div><!-- /input-group -->
            </form>

          </div>
          
        </div>
      </div>      
    </section>

    <section class="rights">
      <div class="container"> 
        <p>
          Copyright &#169; <span id="copyright-year"></span> -
          PT Standardpen Industries
        </p>          
      </div> 
    </section>    
  </footer>
  </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->         
    <script src="js/bootstrap.min.js"></script>
    <script src="js/tm-scripts.js"></script>    
  <!-- </script> -->


  </body>
</html>
